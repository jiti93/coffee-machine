<?php
namespace App\Service\Payment;

interface ChangeInterface
{
    public function validateChange(int $changeAmount): bool;
    public function getChangeMessage(int $changeAmount): string;
}
