<?php
namespace App\Service\Payment;

use App\Entity\Constant\PaymentMethod;
use App\Entity\Constant\BanknoteType;
use App\Entity\Product;
use App\Service\ChangeService;
use App\Service\TransactionService;
use App\Validator\ValidatorInterface;

class CashPaymentService implements PaymentInterface
{
    /**
     * @var ValidatorInterface
     */
    private $changeValidator;

    /**
     * @var ChangeService
     */
    private $changeService;
    /**
     * @var TransactionService
     */
    private $transactionService;

    public function __construct(
        ValidatorInterface $changeValidator,
        TransactionService $transactionService,
        ChangeService $changeService
    ) {
        $this->changeValidator = $changeValidator;
        $this->changeService = $changeService;
        $this->transactionService = $transactionService;
    }

    public function pay(Product $product, int $quantity, int $cost, int $enteredBanknoteValue = 0): void
    {
        $this->transactionService->addTransaction(
            $product,
            PaymentMethod::CASH,
            $quantity,
            $cost,
            $this->calculateTransactionBanknotes($enteredBanknoteValue, $cost)
        );
    }

    public function useBanknotes(): bool
    {
        return true;
    }

    public function validateChange(int $changeAmount): bool
    {
        if (!$changeAmount) {
            return true;
        }

        return $this->changeValidator->validate($changeAmount);
    }

    public function getChangeBanknotes(int $changeAmount): array
    {
        if (!$changeAmount) {
            return [];
        }
        return $this->changeService->countBanknotes($changeAmount);
    }

    public function getChangeMessage(int $changeAmount): string
    {
        if (!$changeAmount) {
            return '';
        }

        return $this->changeService->getChangeMessage($changeAmount);
    }

    private function calculateTransactionBanknotes(int $enteredBanknoteValue, int $cost): array
    {
        $banknotes = [
            BanknoteType::IN => [
                $enteredBanknoteValue
            ]
        ];
        $change = $enteredBanknoteValue - $cost;
        if ($change) {
            $changeBanknotes = $this->changeService->countBanknotes($change);
            $banknotes[BanknoteType::OUT] = [];
            foreach ($changeBanknotes as $banknote => $unit) {
                for($i = 0; $i < $unit; $i++) {
                    $banknotes[BanknoteType::OUT][] = $banknote;
                }
            }
        }

        return $banknotes;
    }
}
