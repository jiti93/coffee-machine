<?php
namespace App\Service\Payment;

use App\Entity\Constant\PaymentMethod;

class PaymentFactory implements PaymentFactoryInterface
{
    /**
     * @var CashPaymentService
     */
    private $cashPaymentService;

    /**
     * @var CardPaymentService
     */
    private $cardPaymentService;

    public function __construct(CashPaymentService $cashPaymentService, CardPaymentService $cardPaymentService)
    {
        $this->cashPaymentService = $cashPaymentService;
        $this->cardPaymentService = $cardPaymentService;
    }

    public function getPaymentServiceByPaymentMethod(string $paymentMethod): ?PaymentInterface
    {
        switch ($paymentMethod) {
            case PaymentMethod::CARD:
                return $this->cardPaymentService;
            case PaymentMethod::CASH:
                return $this->cashPaymentService;
            default:
                return null;
        }
    }
}
