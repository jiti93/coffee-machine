<?php

namespace App\Service\Payment;

interface PaymentFactoryInterface
{
    public function getPaymentServiceByPaymentMethod(string $paymentMethod): ?PaymentInterface;
}
