<?php

namespace App\Service\Payment;

use App\Entity\Product;

interface PaymentInterface extends ChangeInterface
{
    public function useBanknotes(): bool;
    public function pay(Product $product, int $quantity, int $cost, int $enteredBanknoteValue = 0): void;
}
