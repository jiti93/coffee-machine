<?php
namespace App\Service\Payment;

use App\Entity\Constant\PaymentMethod;
use App\Entity\Product;
use App\Service\TransactionService;

class CardPaymentService implements PaymentInterface
{

    /**
     * @var TransactionService
     */
    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function pay(Product $product, int $quantity, int $cost, int $enteredBanknoteValue = 0): void
    {
        $this->transactionService->addTransaction(
            $product,
            PaymentMethod::CARD,
            $quantity,
            $cost
        );
    }

    function useBanknotes(): bool
    {
        return false;
    }

    function validateChange(int $changeAmount): bool
    {
        return true;
    }

    function getChangeMessage(int $changeAmount): string
    {
        return '';
    }
}
