<?php
namespace App\Service;

use App\Entity\Constant\Recipe;
use App\Entity\Product;
use App\Repository\ProductRepository;

class ProductService
{

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var array | null
     */
    private $products;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    public function getProducts(): array
    {
        if (!$this->products) {
            $this->products = $this->productRepository->findAll();

            foreach ($this->products as $product) {
                $this->calculateAvailabilityAndQuantity($product);
            }
        }

        return $this->products;
    }

    public function getProductNames(): array
    {
       $names = [];

       if ($this->products) {
           $this->getProducts();
       }

       foreach ($this->products as $product) {
           $names[] = $product->getName();
       }

       return $names;
    }

    public function getByName(string $productName): ?Product
    {
        if ($this->products) {
            $this->getProducts();
        }

        foreach ($this->products as $product) {
            if ($product->getName() === $productName) {
                return $product;
            }
        }

        return null;
    }

    public function calculateAvailabilityAndQuantity(Product $product): void
    {
        $recipe = Recipe::LIST[$product->getName()];
        $availability = true;
        $smallestProductQuantity = 0;

        foreach ($product->getIngredients() as $ingredient) {
            $necessaryIngredientQuantity = $recipe[$ingredient->getName()];
            $currentIngredientQuantity = $ingredient->getQuantity();

            if ($currentIngredientQuantity < $necessaryIngredientQuantity) {
                $availability = false;
                break;
            }

            $productQuantity = $currentIngredientQuantity / $necessaryIngredientQuantity;
            // always store the smallest quantity
            // e.g if we have milk only for 3 coffees and water for 10 coffees,
            // set the quantity of the product quantity with 3
            if (($productQuantity < $smallestProductQuantity) || !$smallestProductQuantity) {
                $smallestProductQuantity = $productQuantity;
            }
        }

        $product->setAvailability($availability);
        $product->setQuantity($availability ? $smallestProductQuantity : 0);
    }
}
