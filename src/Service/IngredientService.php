<?php

namespace App\Service;

use App\Entity\Constant\Recipe;
use App\Entity\Product;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;

class IngredientService
{
    /**
     * @var IngredientRepository
     */
    private $ingredientRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(IngredientRepository $ingredientRepository, EntityManagerInterface $entityManager)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->entityManager = $entityManager;
    }

    public function recalculateQuantity(Product $product, int $purchasedQuantity): void
    {
        $recipe = Recipe::LIST[$product->getName()];
        foreach ($product->getIngredients() as $ingredient) {
            $ingredientCurrentQuantity = $ingredient->getQuantity();
            $ingredientRecipeQuantity = $recipe[$ingredient->getName()] * $purchasedQuantity;

            $quantity = $ingredientCurrentQuantity - $ingredientRecipeQuantity;
            $ingredient->setQuantity($quantity);

            $this->entityManager->persist($ingredient);
        }

        $this->entityManager->flush();
    }
}
