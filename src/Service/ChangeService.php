<?php
namespace App\Service;

use App\Entity\Constant\BanknoteValue;

class ChangeService
{
    /**
     * @var BanknoteValue[]
     */
    private $cachedBanknotes = [];

    /**
     * @var BanknoteService
     */
    private $banknoteService;


    public function __construct(BanknoteService $banknoteService)
    {

        $this->banknoteService = $banknoteService;
    }

    public function getChangeMessage(int $amount): string
    {
        if ($amount) {
            $banknotes = $this->countBanknotes($amount);

            $result = 'through the following banknotes:' . "\n";
            foreach ($banknotes as $banknote => $units) {
                $result .= sprintf('%d x %d RON', $banknote, $units);
                $result .= "\n";
            }

            return $result;
        }

        return '';
    }

    public function countBanknotes(int $amount): array
    {
        if (!$this->cachedBanknotes && $amount) {
            $availableBanknotes = $this->banknoteService->getAvailableBanknotes();

            $banknoteCounter = [];
            foreach ($availableBanknotes as $banknoteValue => $availableQuantity) {
                if ($amount >= $banknoteValue) {
                    $currentBanknote = intval($amount / $banknoteValue);

                    if ($availableQuantity >= $currentBanknote) {
                        $amount = $amount - ($currentBanknote * $banknoteValue);
                        $banknoteCounter[$banknoteValue] = $currentBanknote;
                    }
                }
            }

            $this->cachedBanknotes = $banknoteCounter;
        }

        return $this->cachedBanknotes;
    }
}
