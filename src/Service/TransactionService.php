<?php

namespace App\Service;

use App\Entity\Banknote;
use App\Entity\Constant\BanknoteCurrency;
use App\Entity\Product;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;

class TransactionService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(TransactionRepository $transactionRepository, EntityManagerInterface $entityManager)
    {

        $this->transactionRepository = $transactionRepository;
        $this->entityManager = $entityManager;
    }

    public function addTransaction(
        Product $product,
        string $paymentMethod,
        int $quantity,
        int $totalAmount,
        array $banknoteTypes = []
    ) {
        $transaction = new Transaction();

        $transaction->setProduct($product);
        $transaction->setPaymentMethod($paymentMethod);
        $transaction->setQuantity($quantity);
        $transaction->setAmount($totalAmount);

        foreach ($banknoteTypes as $banknoteType => $banknotes) {
            foreach ($banknotes as $banknoteValue) {
                $banknote = new Banknote();
                $banknote->setValue($banknoteValue);
                $banknote->setType($banknoteType);
                $banknote->setCurrency(BanknoteCurrency::RON);
                $transaction->addBanknote($banknote);
            }
        }

        $this->entityManager->persist($transaction);
        $this->entityManager->flush();
    }
}
