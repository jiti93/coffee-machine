<?php
namespace App\Service;

use App\Repository\BanknoteRepository;

class BanknoteService
{
    /**
     * @var BanknoteRepository
     */
    private $banknoteRepository;

    public function __construct(BanknoteRepository $banknoteRepository)
    {

        $this->banknoteRepository = $banknoteRepository;
    }

    public function getAvailableBanknotes(): array
    {
        $banknotes = [];
        $banknotesGroupedByValue = $this->banknoteRepository->findAllAndGroupByValue();
        foreach ($banknotesGroupedByValue as $banknote) {
            $banknotes[$banknote['value']] = (int)$banknote['availability'];
        }

        return $banknotes;
    }
}
