<?php
namespace App\Validator;

use App\Service\ChangeService;

class ChangeValidator implements ValidatorInterface
{

    /**
     * @var ChangeService
     */
    private $changeService;

    public function __construct(ChangeService $changeService)
    {
        $this->changeService = $changeService;
    }

    public function validate(int $amount): bool
    {
        if (!$amount) {
            return true;
        }

        $change = $this->changeService->countBanknotes($amount);
        return $change ? true : false;
    }
}
