<?php
namespace App\Repository;

use App\Entity\Constant\BanknoteType;
use App\Entity\Banknote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Banknote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Banknote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Banknote[]    findAll()
 * @method Banknote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BanknoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Banknote::class);
    }

    /**
     *
     */
    public function findAllAndGroupByValue(): array
    {
        $availableValueInSql = $this->getAvailableValueSqlConditionedByType(BanknoteType::IN);
        $totalInBanknotesAlias = sprintf('%s as totalInBanknotes', $availableValueInSql);

        $availableValueOutSql = $this->getAvailableValueSqlConditionedByType(BanknoteType::OUT);
        $totalOutBanknotesAlias = sprintf('%s as totalOutBanknotes', $availableValueOutSql);

        $availabilityAlias = sprintf('(%s-%s) as availability', $availableValueInSql, $availableValueOutSql);

        return $this->createQueryBuilder('b')
            ->select([
                'b.value',
                $totalInBanknotesAlias,
                $totalOutBanknotesAlias,
                $availabilityAlias
            ])
            ->orderBy('b.value', 'DESC')
            ->groupBy('b.value')
            ->getQuery()
            ->execute()
        ;
    }

    private function getAvailableValueSqlConditionedByType(string $type): string
    {
        $value = sprintf('SUM(CASE b.type WHEN \'%s\' THEN b.value ELSE 0 END)', $type);
        return sprintf('(%s/b.value)', $value);
    }
}
