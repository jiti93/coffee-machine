<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * This can be set with:
     *   - Card
     *   - Cash
     *
     * @ORM\Column(type="string", length=255)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $amount;

    /**
     * @ORM\OneToMany(targetEntity=Banknote::class, mappedBy="transaction", cascade={"persist"})
     */
    private $banknotes;

    public function __construct()
    {
        $this->banknotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return Collection|Banknote[]
     */
    public function getBanknotes(): Collection
    {
        return $this->banknotes;
    }

    public function addBanknote(Banknote $banknote): self
    {
        if (!$this->banknotes->contains($banknote)) {
            $this->banknotes[] = $banknote;
            $banknote->setTransaction($this);
        }

        return $this;
    }

    public function removeBanknote(Banknote $banknote): self
    {
        if ($this->banknotes->removeElement($banknote)) {
            // set the owning side to null (unless already changed)
            if ($banknote->getTransaction() === $this) {
                $banknote->setTransaction(null);
            }
        }

        return $this;
    }
}
