<?php

namespace App\Entity;

use App\Repository\BanknoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Constant\BanknoteCurrency;

/**
 * @ORM\Entity(repositoryClass=BanknoteRepository::class)
 */
class Banknote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * This can be set with:
     *   - 1
     *   - 5
     *   - 10
     *
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     *
     * This can be set with:
     *   - in - represents banknotes that is in the machine
     *   - out - represents banknotes that was given as the change from the machine
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Transaction::class, inversedBy="banknotes")
     */
    private $transaction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default" : BanknoteCurrency::RON})
     */
    private $currency;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Transaction $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
