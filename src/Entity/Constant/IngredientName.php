<?php

namespace App\Entity\Constant;

class IngredientName
{
    const COFFEE    = 'Coffee';
    const SUGAR     = 'Sugar';
    const WATER     = 'Water';
    const MILK      = 'Milk';
}