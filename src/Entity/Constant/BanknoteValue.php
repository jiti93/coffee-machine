<?php
namespace App\Entity\Constant;

class BanknoteValue
{
    const ONE_RON   = '1';
    const FIVE_RON  = '5';
    const TEN_RON   = '10';

    const LIST_RON = [
        self::ONE_RON,
        self::FIVE_RON,
        self::TEN_RON
    ];
}
