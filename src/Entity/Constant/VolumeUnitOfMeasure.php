<?php
namespace App\Entity\Constant;

class VolumeUnitOfMeasure
{
    const GRAM = 'g';
    const MILLILITER = 'ml';
}
