<?php

namespace App\Entity\Constant;

class Recipe
{
    const ESPRESSO = [
        IngredientName::COFFEE   => 5,
        IngredientName::SUGAR    => 2,
        IngredientName::WATER    => 30
    ];

    const COFFEE = [
        IngredientName::COFFEE   => 15,
        IngredientName::SUGAR    => 1,
        IngredientName::WATER    => 30
    ];

    const CAPPUCCINO = [
        IngredientName::COFFEE   => 3,
        IngredientName::SUGAR    => 5,
        IngredientName::WATER    => 10,
        IngredientName::MILK     => 10
    ];

    const LATTE_MACCHIATO = [
        IngredientName::COFFEE    => 2,
        IngredientName::SUGAR     => 10,
        IngredientName::WATER     => 10,
        IngredientName::MILK      => 20
    ];

    const LIST = [
        ProductName::ESPRESSO => self::ESPRESSO,
        ProductName::COFFEE => self::COFFEE,
        ProductName::CAPPUCCINO => self::CAPPUCCINO,
        ProductName::LATTE_MACCHIATO => self::LATTE_MACCHIATO,
    ];
}