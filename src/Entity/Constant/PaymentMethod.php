<?php

namespace App\Entity\Constant;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class PaymentMethod
{
    const CASH = 'Cash';
    const CARD = 'Card';

    const LIST = [
        self::CASH,
        self::CARD
    ];
}
