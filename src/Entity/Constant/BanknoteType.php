<?php
namespace App\Entity\Constant;

class BanknoteType
{
    const IN   = 'in';
    const OUT  = 'out';
}
