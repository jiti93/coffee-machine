<?php

namespace App\Entity\Constant;

class ProductName
{
    const ESPRESSO           = 'Espresso';
    const COFFEE             = 'Coffee';
    const CAPPUCCINO         = 'Cappuccino';
    const LATTE_MACCHIATO    = 'Latte macchiato';
}
