<?php
namespace App\DataFixtures;

use App\Entity\Constant\IngredientName;
use App\Entity\Constant\VolumeUnitOfMeasure;
use App\Entity\Ingredient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class IngredientFixtures extends Fixture
{

    /** @var ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->addIngredient(IngredientName::COFFEE, 50,VolumeUnitOfMeasure::GRAM);
        $this->addIngredient(IngredientName::SUGAR, 50, VolumeUnitOfMeasure::GRAM);
        $this->addIngredient(IngredientName::WATER, 300,VolumeUnitOfMeasure::MILLILITER);
        $this->addIngredient(IngredientName::MILK, 300,VolumeUnitOfMeasure::MILLILITER);

        $manager->flush();
    }

    private function addIngredient(string $name, int $quantity, string $volumeUnitOfMeasure): void
    {
        $ingredient = new Ingredient();
        $ingredient->setName($name);
        $ingredient->setQuantity($quantity);
        $ingredient->setVolumeUnitOfMeasure($volumeUnitOfMeasure);

        $this->manager->persist($ingredient);
    }
}
