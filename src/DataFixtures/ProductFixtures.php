<?php

namespace App\DataFixtures;

use App\Entity\Constant\Recipe;
use App\Entity\Product;
use App\Repository\IngredientRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{

    /**
     * @var IngredientRepository
     */
    private $ingredientRepository;

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(IngredientRepository $ingredientRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $recipes = Recipe::LIST;
        foreach ($recipes as $productName => $ingredientNames) {
            $ingredients = $this->ingredientRepository->findByNames(array_keys($ingredientNames));
            $this->addProduct($productName, rand (1, 2), $ingredients);
        }

        $this->manager->flush();
    }

    private function addProduct(string $name, float $price, array $ingredients): void
    {
        $product = new Product();
        $product->setName($name);
        $product->setPrice($price);
        foreach ($ingredients as $ingredient) {
            $product->addIngredient($ingredient);
        }

        $this->manager->persist($product);
    }
}
