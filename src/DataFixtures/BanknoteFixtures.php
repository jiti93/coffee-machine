<?php
namespace App\DataFixtures;

use App\Entity\Banknote;
use App\Entity\Constant\BanknoteCurrency;
use App\Entity\Constant\BanknoteType;
use App\Entity\Constant\BanknoteValue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use \Exception;

class BanknoteFixtures extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $banknoteValues = BanknoteValue::LIST_RON;

        foreach ($banknoteValues as $banknoteValue) {
            try {
                for ($i = 0; $i < random_int(5, 10); $i++) {
                    $this->addBanknote($banknoteValue);
                }
            } catch (Exception $e) {
                $this->addBanknote($banknoteValue);
            }
        }

        $this->manager->flush();
    }

    private function addBanknote(int $value)
    {
        $banknote = new Banknote();
        $banknote->setValue($value);
        $banknote->setType(BanknoteType::IN);
        $banknote->setCurrency(BanknoteCurrency::RON);

        $this->manager->persist($banknote);
    }
}
