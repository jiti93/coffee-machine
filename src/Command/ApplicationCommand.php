<?php
namespace App\Command;

use App\Entity\Product;
use App\Service\IngredientService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \Exception;

class ApplicationCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'app:coffee_machine';

    /**
     * @var IngredientService
     */
    private $ingredientService;

    public function __construct(IngredientService $ingredientService)
    {
        $this->ingredientService = $ingredientService;
        parent::__construct(ApplicationCommand::$defaultName);
    }

    protected function configure()
    {
        $this->setDescription("Outputs Hello World");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln('The machine is used by someone else. Please try again later.');

            return Command::SUCCESS;
        }

        $this->runProductCommand($input, $output);
        $this->runPaymentCommand($input, $output);

        /** @var Product $selectedProduct */
        $selectedProduct = $input->getArgument('product');
        /** @var int $quantity */
        $quantity = $input->getArgument('quantity');

        $this->ingredientService->recalculateQuantity($selectedProduct, $quantity);

        $output->writeln('Your coffee will be ready soon please wait a moment.');
        $this->showProgressBar($output);
        $output->writeln('Your coffee is ready. Thank you.');

        return Command::SUCCESS;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    private function runProductCommand(InputInterface $input, OutputInterface $output): void
    {
        $productCommand = $this->getApplication()->find('products');
        $productCommand->run($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    private function runPaymentCommand(InputInterface $input, OutputInterface $output): void
    {
        /** @var Product $selectedProduct */
        $selectedProduct = $input->getArgument('product');

        $arguments = [
            'product' => $selectedProduct,
            'quantity' => $input->getArgument('quantity'),
        ];
        $paymentInput = new ArrayInput($arguments);
        $paymentCommand = $this->getApplication()->find('payment');
        $paymentCommand->run($paymentInput, $output);
    }

    private function showProgressBar(OutputInterface $output)
    {
        $output->writeln(
            ''
        );
        $section1 = $output->section();

        $progress1 = new ProgressBar($section1);

        $progress1->start(100);

        $i = 0;
        while (++$i < 100) {
            $progress1->advance();
            usleep(50000);
        }

        $output->writeln(
            ''
        );
    }
}