<?php
namespace App\Command;

use App\Entity\Constant\BanknoteValue;
use App\Entity\Constant\PaymentMethod;
use App\Entity\Product;
use App\Service\Payment\PaymentInterface;
use App\Service\Payment\PaymentFactoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use \RuntimeException;

class PaymentCommand extends Command
{
    protected static $defaultName = 'payment';

    /**
     * @var PaymentFactoryInterface
     */
    private $paymentFactory;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var InputInterface
     */
    private $input;

    /** @var int */
    private $enteredBanknoteValue = 0;

    /**
     * @var int
     */
    private $change = 0;

    /**
     * @var string
     */
    private $changeMessage = '';

    public function __construct(PaymentFactoryInterface $paymentFactory)
    {
        $this->paymentFactory = $paymentFactory;

        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription("Payment");
        $this->addArgument('product', InputArgument::REQUIRED, 'The selected product.');
        $this->addArgument('quantity', InputArgument::REQUIRED, 'The entered quantity');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        /** @var Product $product */
        $product =  $this->input->getArgument('product');
        $quantity = $this->input->getArgument('quantity');
        $cost = $product->getPrice() * $quantity;
        $paymentMethod = $this->showPaymentMethodChoice();

        $paymentService = $this->paymentFactory->getPaymentServiceByPaymentMethod($paymentMethod);
        if ($paymentService->useBanknotes()) {
            $this->showBanknotesChoiceAndValidate($cost, $paymentService);
        }
        $paymentService->pay($product, $quantity, $cost, $this->enteredBanknoteValue);

        $this->showPaymentMessage($cost);
        return self::SUCCESS;
    }

    protected function showBanknotesChoiceAndValidate(int $cost, PaymentInterface $paymentService): void
    {
        $this->enteredBanknoteValue = $this->showBanknotesChoice($cost);
        $this->change = $this->enteredBanknoteValue - $cost;

        if (($this->change > 0)
            && !$paymentService->validateChange($this->change)
        ) {
            $this->output->writeln('The machine does not have the necessary banknotes to return the change.');
            $this->showBanknotesChoiceAndValidate($cost, $paymentService);
        }

        $this->changeMessage = $paymentService->getChangeMessage($this->change);
    }

    protected function showPaymentMessage(int $cost): void
    {
        $message ="\n";
        $message .= sprintf(
            'Your purchase costing %d RON.',
            $cost
        );

        if ($this->enteredBanknoteValue) {
            $message = sprintf('%s You introduced a %d RON banknote', $message, $this->enteredBanknoteValue);
        }

        if ($this->change) {
            $message .="\n";
            $message = sprintf('%s%d RON were returned to you %s', $message, $this->change, $this->changeMessage);
            $this->output->writeln('');
            $message .="\n";
        }

        $this->output->writeln($message);
    }

    protected function showPaymentMethodChoice(): string
    {
        $availablePaymentMethods = PaymentMethod::LIST;
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select the payment method:',
            $availablePaymentMethods,
            PaymentMethod::CARD
        );

        $question->setValidator(function ($answer) use ($availablePaymentMethods) {
            if (isset($availablePaymentMethods[$answer])) {
                $answer = $availablePaymentMethods[$answer];
            }
            if (!in_array($answer, $availablePaymentMethods, true)) {
                throw new RuntimeException(
                    'The entered payment method is not supported. Please try again.'
                );
            }

            return $answer;
        });

        return $helper->ask($this->input, $this->output, $question);
    }

    protected function showBanknotesChoice(int $cost): string
    {
        $availableBanknote = BanknoteValue::LIST_RON;

        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Please insert a RON banknote',
            $availableBanknote
        );

        $question->setValidator(function ($answer) use ($availableBanknote, $cost) {
            if (isset($availableBanknote[$answer])) {
                $answer = $availableBanknote[$answer];
            }

            if (!in_array($answer, $availableBanknote, true)) {
                throw new RuntimeException(
                    'The entered banknote is not supported. Please insert again.'
                );
            }

            if ($answer < $cost) {
                throw new RuntimeException(
                    sprintf(
                        'The amount entered must be greater than %d RON. Please insert other banknote.',
                        $cost
                    )
                );
            }
            return $answer;
        });

        return $helper->ask($this->input, $this->output, $question);
    }
}
