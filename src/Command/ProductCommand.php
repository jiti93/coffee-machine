<?php
namespace App\Command;

use App\Entity\Product;
use App\Service\ProductService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use RuntimeException;

class ProductCommand extends Command
{
    const HEADERS = [
        'Name',
        'Price',
        'Availability',
        'Quantity',
        'Ingredients'
    ];

    protected static $defaultName = 'products';

    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        parent::__construct();
        $this->productService = $productService;
    }

    protected function configure(): void
    {
        $this->setDescription("Show a product table and allow to select a product");
        $this->addArgument('product', InputArgument::OPTIONAL, 'The selected product.');
        $this->addArgument('quantity', InputArgument::OPTIONAL, 'The entered quantity.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $products = $this->productService->getProducts();
        $this->showProductsTable($products, $output);

        $product = $this->showSelectProductChoice($input, $output);
        $input->setArgument('product', $product);

        $quantity = $this->insertQuantity($product, $input, $output);
        $input->setArgument('quantity', $quantity);

        return self::SUCCESS;
    }

    private function showProductsTable(array $products, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setHeaders(self::HEADERS)
            ->setRows($this->getRows($products));

        $table->setStyle('borderless');
        $table->render();
    }

    private function insertQuantity(Product $product, InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $question = new Question('Please enter the desired quantity:', 1);
        $question->setValidator(function (int $answer) use ($product) {
            if ($answer <= 0){
                throw new RuntimeException(
                    'The quantity must be greater than 0. Please enter again.'
                );
            }

            if ($answer > $product->getQuantity()) {
                throw new RuntimeException(
                    sprintf('The quantity must be lower than %d. Please enter again.', $product->getQuantity())
                );
            }

            return $answer;
        });

        return $helper->ask($input, $output, $question);
    }

    private function showSelectProductChoice(InputInterface $input, OutputInterface $output): ?Product
    {
        $helper = $this->getHelper('question');
        $availableProductNames = $this->productService->getProductNames();

        $question = new ChoiceQuestion('Please select a product:', $availableProductNames);
        $question->setValidator(function ($answer) use ($availableProductNames) {
            if (isset($availableProductNames[$answer])){
                $answer = $availableProductNames[$answer];
            }

            if (!in_array($answer, $availableProductNames, true)) {
                throw new RuntimeException(
                    'The entered product is not correct. Please select again.'
                );
            }

            return $answer;
        });

        $productName = $helper->ask($input, $output, $question);
        $output->writeln(sprintf('You have just selected: %s', $productName));
        return $this->productService->getByName($productName);
    }

    private function getRows(array $products): array
    {
        $rows = [];
        foreach ($products as $product) {
            $rows[] = $this->getRowDetails($product);
        }

        return $rows;
    }

    private function getRowDetails(Product $product): array
    {
        $content = [];
        foreach ($product->getIngredients() as $ingredient) {
            $content[]= strtolower($ingredient->getName());
        }

        return [
            $product->getName(),
            sprintf('%.1f RON', $product->getPrice()),
            $product->isAvailability() ? 'Yes' : 'No',
            sprintf('%d units', $product->getQuantity()),
            implode(', ', $content)
        ];
    }
}
