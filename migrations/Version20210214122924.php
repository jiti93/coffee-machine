<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210214122924 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE banknote (id INT AUTO_INCREMENT NOT NULL, transaction_id INT DEFAULT NULL, value INT NOT NULL, type VARCHAR(255) NOT NULL, currency VARCHAR(255) DEFAULT \'RON\', INDEX IDX_E8C832802FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredient (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, quantity INT NOT NULL, volume_unit_of_measure VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredient_product (ingredient_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_D27D0F6B933FE08C (ingredient_id), INDEX IDX_D27D0F6B4584665A (product_id), PRIMARY KEY(ingredient_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, payment_method VARCHAR(255) NOT NULL, quantity INT NOT NULL, amount INT NOT NULL, INDEX IDX_723705D14584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE banknote ADD CONSTRAINT FK_E8C832802FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE ingredient_product ADD CONSTRAINT FK_D27D0F6B933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ingredient_product ADD CONSTRAINT FK_D27D0F6B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D14584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient_product DROP FOREIGN KEY FK_D27D0F6B933FE08C');
        $this->addSql('ALTER TABLE ingredient_product DROP FOREIGN KEY FK_D27D0F6B4584665A');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D14584665A');
        $this->addSql('ALTER TABLE banknote DROP FOREIGN KEY FK_E8C832802FC0CB0F');
        $this->addSql('DROP TABLE banknote');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE ingredient_product');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE transaction');
    }
}
