### How to start docker? ###

The following commands in the root of the project should be ran:
* to **build** and **start** the containers from docker-compose file:
    ```sh 
   docker-compose up --build  
* to end up the container
  ```sh 
    docker-compose down
* see all containers with all images (running or not)
  ```sh 
    docker ps -a
* open php container
  ```sh 
    docker exec -it coffee_machine_php sh
Please note that some commands (like run migrations, execute fixture) were added in the *docker/php/docker-entrypoint.sh* and these will be ran when the 'APP_ENV' from *Dockerfile* file is set with 'dev'.
If something went wrong, set the value of 'APP_ENV' from *Dockerfile* file as **prod** and than run the following commands:
* Create database
    ```sh 
    php bin/console doctrine:database:create
* Run migrations
    ```sh 
    php bin/console doctrine:migrations:migrate
* Run fixtures in order to populate  the products, the ingredients and the banknotes table
   ```sh 
    php bin/console doctrine:fixtures:load
    
### How to use Coffee Machine application? ###
In order to use the CLI application the follwoing command should be ran:
```sh
php bin/console app:coffee_machine
